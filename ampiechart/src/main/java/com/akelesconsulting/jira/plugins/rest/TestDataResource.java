package com.akelesconsulting.jira.plugins.rest;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This REST resource is called by the gadget.xml's ajax call to retrieve the results. It will return the average temperature of each country
 * for the month
 */
@Path("/data")
public class TestDataResource
{

    private final Logger logger = Logger.getLogger(TestDataResource.class);

    public TestDataResource()
    {
        logger.setLevel(Level.DEBUG);
    }


    /**
     * The results can be viewed from http://127.0.0.1:2990/jira/rest/piechartresource/1.0/data
     * @return a JSON response in the format
     * [ { "country": "Singapore", "temp": 32} , { "country": "Canada", "temp": 10},
     *   { "country": "India", "temp": 39}, { "country": "Japan", "temp": 18} ]
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getData()

    {
        JSONArray temperatureArray = new JSONArray();
        try
        {
            JSONObject singapore = new JSONObject();
            singapore.put("country", "Singapore");
            singapore.put("temp", 32);

            JSONObject canada = new JSONObject();
            canada.put("country", "Canada");
            canada.put("temp", 10);

            JSONObject india = new JSONObject();
            india.put("country", "India");
            india.put("temp", 39);

            JSONObject japan = new JSONObject();
            japan.put("country", "Japan");
            japan.put("temp", 18);

            temperatureArray.put(singapore);
            temperatureArray.put(canada);
            temperatureArray.put(india);
            temperatureArray.put(japan);
        }
        catch (JSONException jex)
        {
            logger.error("Error with JSON");

        }
       return Response.ok(temperatureArray.toString(), MediaType.APPLICATION_JSON_TYPE).build();
    }
}
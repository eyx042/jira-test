(function ()
{
    var gadget = AJS.Gadget({
        baseUrl: AJS.$('#baseURL').val(), // Get baseUrl from hidden input field in template in gadget.xml
        useOauth: "/rest/gadget/1.0/currentUser",
        view: {
            onResizeAdjustHeight: true,
            enableReload: true,
            template: function (args)
            {
                var pieChart = AmCharts.makeChart( "chart-div", {
                    "type": "pie",
                    "dataProvider": args.data,
                    "titleField": "country",
                    "valueField": "temp",
                });
            },
            args: [
                {
                    key: "data",
                    ajaxOptions: function ()
                    {
                        return {
                            url: "/rest/piechartresource/1.0/data",
                        };
                    }
                }
            ]
        }
    });
})();